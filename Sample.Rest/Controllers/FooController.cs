﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sample.Rest.Controllers
{
    [EnableCors(origins: "http://localhost:8000", headers: "*", methods: "*")]
    public class FooController : ApiController
    {
        // GET: api/Foo
        public IEnumerable<string> Get()
        {
            return new string[] { "foo", "bar" };
        }

        // GET: api/Foo/5
        public string Get(int id)
        {
            return String.Format("foo {0}", id);
        }

        // POST: api/Foo
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Foo/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Foo/5
        public void Delete(int id)
        {
        }
    }
}
