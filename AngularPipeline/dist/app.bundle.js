webpackJsonp([0],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {


var angular =   __webpack_require__(0);
                __webpack_require__(1);
                __webpack_require__(5);
                
module.exports = angular.module('app', ['ngResource']);

__webpack_require__(4);           


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(6);

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var angular =   __webpack_require__(0);
                __webpack_require__(2);
                __webpack_require__(3);

module.exports = angular.module('ui.app', ['ngRoute', 'app']);
__webpack_require__(12);
__webpack_require__(14);

angular
    .element(function() {
        angular.bootstrap(document, ['ui.app']);
    });


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0).module('app')
    .service('foo', fooService);

fooService.$inject = ['$resource', 'apiEndPoint'];

function fooService ($resource, apiEndPoint) {
    return $resource(apiEndPoint + '/foo');
}

/***/ }),
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */
/***/ (function(module, exports) {

module.exports = "<div>Foo</div>";

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0).module('ui.app')
    .component('fooUi', {
        template: __webpack_require__(10),
        controller:fooUiController
    });

fooUiController.$inject=['foo'];
function fooUiController (foo) {
    foo.get({id:'4'});
}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(11);

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0).module('ui.app')
    .config(function($routeProvider) {
        $routeProvider.when('/', {
            template: '<foo-ui></foo-ui>'
        });
    });

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(13);

/***/ })
],[3]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvYXBwLm1vZHVsZS5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvc2VydmljZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vdWkvdWkubW9kdWxlLmpzIiwid2VicGFjazovLy8uL2FwcC9zZXJ2aWNlcy9mb28uc2VydmljZS5qcyIsIndlYnBhY2s6Ly8vLi91aS9jb21wb25lbnRzL2Zvby11aS9mb28tdWkucGFydGlhbC5odG1sIiwid2VicGFjazovLy8uL3VpL2NvbXBvbmVudHMvZm9vLXVpL2Zvby11aS5jb21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vdWkvY29tcG9uZW50cy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi91aS92aWV3cy9mb28udmlldy5qcyIsIndlYnBhY2s6Ly8vLi91aS92aWV3cy9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSx1Qjs7Ozs7OztBQ1BBLHVCOzs7Ozs7QUNBQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7Ozs7Ozs7QUNYTDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7QUNQQSxrQzs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsQzs7Ozs7O0FDVEEsd0I7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULEtBQUssRTs7Ozs7O0FDTEwsd0IiLCJmaWxlIjoiYXBwLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG52YXIgYW5ndWxhciA9ICAgcmVxdWlyZSgnYW5ndWxhcicpO1xyXG4gICAgICAgICAgICAgICAgcmVxdWlyZSgnYW5ndWxhci1yZXNvdXJjZScpO1xyXG4gICAgICAgICAgICAgICAgcmVxdWlyZSgnLi4vdWkvdWkubW9kdWxlJyk7XHJcbiAgICAgICAgICAgICAgICBcclxubW9kdWxlLmV4cG9ydHMgPSBhbmd1bGFyLm1vZHVsZSgnYXBwJywgWyduZ1Jlc291cmNlJ10pO1xyXG5cclxucmVxdWlyZSgnLi9zZXJ2aWNlcycpOyAgICAgICAgICAgXHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vYXBwL2FwcC5tb2R1bGUuanNcbi8vIG1vZHVsZSBpZCA9IDNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwicmVxdWlyZSgnLi9mb28uc2VydmljZScpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vYXBwL3NlcnZpY2VzL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSA0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBhbmd1bGFyID0gICByZXF1aXJlKCdhbmd1bGFyJyk7XHJcbiAgICAgICAgICAgICAgICByZXF1aXJlKCdhbmd1bGFyLXJvdXRlJyk7XHJcbiAgICAgICAgICAgICAgICByZXF1aXJlKCcuLi9hcHAvYXBwLm1vZHVsZScpO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBhbmd1bGFyLm1vZHVsZSgndWkuYXBwJywgWyduZ1JvdXRlJywgJ2FwcCddKTtcclxucmVxdWlyZSgnLi9jb21wb25lbnRzJyk7XHJcbnJlcXVpcmUoJy4vdmlld3MnKTtcclxuXHJcbmFuZ3VsYXJcclxuICAgIC5lbGVtZW50KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGFuZ3VsYXIuYm9vdHN0cmFwKGRvY3VtZW50LCBbJ3VpLmFwcCddKTtcclxuICAgIH0pO1xyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3VpL3VpLm1vZHVsZS5qc1xuLy8gbW9kdWxlIGlkID0gNVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJyZXF1aXJlKCdhbmd1bGFyJykubW9kdWxlKCdhcHAnKVxyXG4gICAgLnNlcnZpY2UoJ2ZvbycsIGZvb1NlcnZpY2UpO1xyXG5cclxuZm9vU2VydmljZS4kaW5qZWN0ID0gWyckcmVzb3VyY2UnLCAnYXBpRW5kUG9pbnQnXTtcclxuXHJcbmZ1bmN0aW9uIGZvb1NlcnZpY2UgKCRyZXNvdXJjZSwgYXBpRW5kUG9pbnQpIHtcclxuICAgIHJldHVybiAkcmVzb3VyY2UoYXBpRW5kUG9pbnQgKyAnL2ZvbycpO1xyXG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9hcHAvc2VydmljZXMvZm9vLnNlcnZpY2UuanNcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxkaXY+Rm9vPC9kaXY+XCI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi91aS9jb21wb25lbnRzL2Zvby11aS9mb28tdWkucGFydGlhbC5odG1sXG4vLyBtb2R1bGUgaWQgPSAxMFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJyZXF1aXJlKCdhbmd1bGFyJykubW9kdWxlKCd1aS5hcHAnKVxyXG4gICAgLmNvbXBvbmVudCgnZm9vVWknLCB7XHJcbiAgICAgICAgdGVtcGxhdGU6IHJlcXVpcmUoJy4vZm9vLXVpLnBhcnRpYWwuaHRtbCcpLFxyXG4gICAgICAgIGNvbnRyb2xsZXI6Zm9vVWlDb250cm9sbGVyXHJcbiAgICB9KTtcclxuXHJcbmZvb1VpQ29udHJvbGxlci4kaW5qZWN0PVsnZm9vJ107XHJcbmZ1bmN0aW9uIGZvb1VpQ29udHJvbGxlciAoZm9vKSB7XHJcbiAgICBmb28uZ2V0KHtpZDonNCd9KTtcclxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vdWkvY29tcG9uZW50cy9mb28tdWkvZm9vLXVpLmNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gMTFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwicmVxdWlyZSgnLi9mb28tdWkvZm9vLXVpLmNvbXBvbmVudCcpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vdWkvY29tcG9uZW50cy9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMTJcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwicmVxdWlyZSgnYW5ndWxhcicpLm1vZHVsZSgndWkuYXBwJylcclxuICAgIC5jb25maWcoZnVuY3Rpb24oJHJvdXRlUHJvdmlkZXIpIHtcclxuICAgICAgICAkcm91dGVQcm92aWRlci53aGVuKCcvJywge1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzxmb28tdWk+PC9mb28tdWk+J1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi91aS92aWV3cy9mb28udmlldy5qc1xuLy8gbW9kdWxlIGlkID0gMTNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwicmVxdWlyZSgnLi9mb28udmlldycpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vdWkvdmlld3MvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDE0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCJdLCJzb3VyY2VSb290IjoiIn0=