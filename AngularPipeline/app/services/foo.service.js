require('angular').module('app')
    .service('foo', fooService);

fooService.$inject = ['$resource', 'apiEndPoint'];

function fooService ($resource, apiEndPoint) {
    return $resource(apiEndPoint + '/foo');
}