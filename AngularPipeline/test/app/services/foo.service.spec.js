describe('foo service', function () {
    var $httpBackend, foo;
    beforeEach(angular.mock.module('ui.app'));
    beforeEach(inject(function (_$httpBackend_, _foo_) {
        $httpBackend = _$httpBackend_;
        foo = _foo_;
    }));
    it('has CRUD operations', function () {
        expect(foo.get).to.be.a('function');
        expect(foo.query).to.be.a('function');
        expect(foo.save).to.be.a('function');
    });
    describe('\'s rest calls:', function () {
        describe('query', function () {
            it('resolves data normally', function () {
                $httpBackend.expectGET('api/foo').respond(200);
                foo.query();
                $httpBackend.flush();
            })
        });
    })
});