var path = require('path'), webpack = require("webpack");
module.exports = {
    entry:{
        app:'./app/app.module.js',
        vendor:['angular','angular-resource', 'angular-route']
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js'
    },
    plugins:[
        new webpack.optimize.CommonsChunkPlugin({name:'vendor',filename:'vendor.bundle.js'})
    ],
    module: {
        rules: [
            {
                test:/\.partial\.html$/i,
                use:{
                    loader:'html-loader'
                }
            }
        ]
    },
    devtool: (!process.env.NODE_ENV || process.env.NODE_ENV === 'development')?'#inline-source-map':false
};