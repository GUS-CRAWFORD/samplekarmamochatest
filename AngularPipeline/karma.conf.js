// Karma configuration
// Generated on Tue Mar 28 2017 15:48:34 GMT-0400 (Eastern Daylight Time)
var path = require('path');
module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha','chai'],

    // pack source and tests with webpack first
    webpack:{
    	devtool: '#inline-source-map',
    	module: {
    		rules: [
          {
            test: /\.partial\.html$/i,
            use: [{ loader: 'html-loader', query: { minimized: true } }]
          },
          // instrument only testing sources with Istanbul
          // "Your index.js files are part of our code and should be tested"
          // - hueniverse (https://github.com/hapijs/lab/issues/120)
          {
              test: /\.js$/,
              include: path.resolve('ui'),
              loader: 'istanbul-instrumenter-loader'
          }        
        ]
    	}
    },
    // list of files / patterns to load in the browser
    files: [
      // sources...
      //'app/**/*.js',
      //'ui/**/*.js'
      // use webpack entry points to load sources though:
      'ui/ui.module.js',
      'test/test.module.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'ui/ui.module.js' : ['webpack'],
      'test/test.module.js' : ['webpack']
    },


    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    // https://github.com/karma-runner/karma-coverage
    reporters: ['progress', 'coverage-istanbul'],

    /*coverageReporter: {
      type : 'html',
      dir : 'coverage/',
      instrumenterOptions: {
        istanbul: { noCompact: true }
      }
    },*/
    coverageIstanbulReporter: {
        reports: [ 'html', 'text-summary' ],
        fixWebpackSourcePaths: true,
        dir: './test/coverage'
    },

    port: 9876,

    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
