require('angular').module('ui.app')
    .component('fooUi', {
        template: require('./foo-ui.partial.html'),
        controller:fooUiController
    });

fooUiController.$inject=['foo'];
function fooUiController (foo) {
    foo.get({id:'4'});
}